<?php

/**
 * @file Google plus admin forms
 */

/**
 * Menu callback, google API settings
 *
 * @return array form element details
 */
function google_plus_login_settings_page($form, &$form_state) {

  $form['google-api-settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
  );
  $form['google-api-settings']['google_plus_login_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_plus_login_client_id', ''),
  );

  $form['google-api-settings']['google_plus_login_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_plus_login_client_secret', ''),
  );

  $form['google-api-settings']['google_plus_login_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('google_plus_login_api_key', ''),
  );

  $form['google-button'] = array(
    '#type' => 'fieldset',
    '#title' => t('G+ connect button behavior'),
  );
  $form['google-button']['google_plus_login_reg_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display G+ connect button on register form'),
    '#default_value' => variable_get('google_plus_login_reg_button', 0),
  );

  $form['google-button']['google_plus_login_connect_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display G+ connect button on profile form and login form'),
    '#default_value' => variable_get('google_plus_login_connect_button', 0),
  );

  return system_settings_form($form);
}

/**
 * Menu callback, google plus field mapping settings
 */
function google_plus_login_field_mapping_settings_page($form, &$form_state) {
  $mapping_options = variable_get('google_plus_login_mapping_settings', array());

  $form['profile_picture'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile picture'),
  );
  $form['profile_picture']['save_user_picture'] = array(
    '#type' => 'checkbox',
    '#title' => t('Import google profile picture'),
    '#default_value' => isset($mapping_options['save_user_picture']) ? $mapping_options['save_user_picture'] : 0,
  );

  $form['field_mapping'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field mapping'),
  );
  foreach (field_info_instances('user', 'user') as $field_name => $field_info) {
    if (in_array($field_info['widget']['type'], array('text_textarea_with_summary', 'text_textfield'))) {
      $options = array(
        '-' => t('Select'),
        'first' => t('First Name [name.givenName]'),
        'last' => t('Last Name [name.familyName]'),
        'display' => t('Display Name [displayName]'),
        'url' => t('Profile url [url]'),
        'email' => t('E-mail [email.0.value]'),
        'town' => t('Last location [placesLived.0.value]'),
      );
    }
    elseif (in_array($field_info['widget']['type'], array('image_image'))) {
      $options = array(
        '-' => t('Select'),
        'image' => t('Profile image [img.url]'),
      );
    }
    elseif (in_array($field_info['widget']['type'], array('options_select'))) {
      $options = array(
        '-' => t('Select'),
        'gender' => t('Gender [gender]'),
      );
    }
    else {
     $options = array('-' => t('Select'));
    }
    $form['field_mapping'][$field_info['field_name']] = array(
      '#type' => 'select',
      '#title' => $field_info['label'],
      '#options' => $options,
      '#default_value' => isset($mapping_options[$field_info['field_name']]) ? $mapping_options[$field_info['field_name']] : '-',
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Menu callback, save google plus field mapping settings
 */
function google_plus_login_field_mapping_settings_page_submit(&$form, &$form_state) {
  variable_set('google_plus_login_mapping_settings', $form_state['values']);
  drupal_set_message(t('Mapping settings have been saved!'));
}